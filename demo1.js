const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://www.dev.leasemgt.xl.co.id/
  await page.goto('https://www.dev.leasemgt.xl.co.id/');

  // Go to https://www.dev.leasemgt.xl.co.id/auth/login
  await page.goto('https://www.dev.leasemgt.xl.co.id/auth/login');

  // Click text=Cognito Login
  await page.click('text=Cognito Login');
  // assert.equal(page.url(), 'https://auth.dev.leasemgt.xl.co.id/login?client_id=d4b0p4uiu0d5605gq3sokulb8&response_type=token&scope=email+openid+profile&redirect_uri=https://www.dev.leasemgt.xl.co.id');

  // Click :nth-match([placeholder="name@host.com"], 2)
  await page.click(':nth-match([placeholder="name@host.com"], 2)');

  // Fill :nth-match([placeholder="name@host.com"], 2)
  await page.fill(':nth-match([placeholder="name@host.com"], 2)', 'lms.leadfiber@yahoo.com');

  // Press Tab
  await page.press(':nth-match([placeholder="name@host.com"], 2)', 'Tab');

  // Fill :nth-match([placeholder="Password"], 2)
  await page.fill(':nth-match([placeholder="Password"], 2)', 'Test_123');

  // Click :nth-match([aria-label="submit"], 2)
  await page.click(':nth-match([aria-label="submit"], 2)');
  // assert.equal(page.url(), 'https://www.dev.leasemgt.xl.co.id/#id_token=eyJraWQiOiJaMkhPTTc2aTVZXC9RNEg4b0tLTGZFdWVKMDhWNVUrNVRWamZWU1wvNTBPUlU9IiwiYWxnIjoiUlMyNTYifQ.eyJjdXN0b206dGl0bGUiOiJMZWFkIEZpYmVyIiwiYXRfaGFzaCI6Im5uWDFQc0hMeVBEZFRQc2lLT0EwLVEiLCJjdXN0b206b3JnYW5pemF0aW9uIjoiWEwiLCJzdWIiOiJlMTk0MDQzNy0zMzBmLTRkMWMtYWExNy00YmMwODNjMDA3ZmQiLCJjdXN0b206Z3JvdXBpZCI6IkxFQURGSUJFUiIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3QtMS5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMV9rR2tDWmQ4TUwiLCJjb2duaXRvOnVzZXJuYW1lIjoiZTE5NDA0MzctMzMwZi00ZDFjLWFhMTctNGJjMDgzYzAwN2ZkIiwiYXVkIjoiZDRiMHA0dWl1MGQ1NjA1Z3Ezc29rdWxiOCIsImV2ZW50X2lkIjoiNDk0ODFiNjctODVlMi00YjljLTk5MTAtZmUwYjc0N2E0MzVlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MzI2NjEyMTEsIm5hbWUiOiJQVCBYTCBBeGlhdGEgVGJrIiwiZXhwIjoxNjMyNjY0ODExLCJpYXQiOjE2MzI2NjEyMTEsImVtYWlsIjoibG1zLmxlYWRmaWJlckB5YWhvby5jb20ifQ.FhkntoqO0DsHYqA-7C0lrYcvkz7mI8zrNpbVcrtSW2MgrJmajVmYd3jb-3laBoALz5fXQS2lh80VdZZwJHlWyFOSEwjXUv2anaRZoN95AK-sNDEYj9YYK9hDRaxWhgF4nv3Pvz7vOjKeeALW0VEmxQkTVR7VvfQP2tr6HoiN2FH_qRmFyhskxfjia4bJURZ3t2Ibn2YSmI66GHyF_1q5lMTusLO-sUMhZHShzCXwLcAG0LpbEhmgQQxuEgYZoYgNTJnQhRHUqOicnQP5V4n9_hLL0wfjV09z0jSiIOS5HW9tprxeLvFf0hvgD1vzd1a012FDWi8dJ146Kdqa5wrjgA&access_token=eyJraWQiOiJZaXEySDlGcG1Tb1JXYU13OFRIYTRLNXpMKzNuaUM2YmNqZGx1ZnYxbkNZPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJlMTk0MDQzNy0zMzBmLTRkMWMtYWExNy00YmMwODNjMDA3ZmQiLCJldmVudF9pZCI6IjQ5NDgxYjY3LTg1ZTItNGI5Yy05OTEwLWZlMGI3NDdhNDM1ZSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwiLCJhdXRoX3RpbWUiOjE2MzI2NjEyMTEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3QtMS5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMV9rR2tDWmQ4TUwiLCJleHAiOjE2MzI2NjQ4MTEsImlhdCI6MTYzMjY2MTIxMSwidmVyc2lvbiI6MiwianRpIjoiZDFjODI1YjctYWU2Yi00NTYzLWI2MzMtODlmYmJiYzMxOTQ0IiwiY2xpZW50X2lkIjoiZDRiMHA0dWl1MGQ1NjA1Z3Ezc29rdWxiOCIsInVzZXJuYW1lIjoiZTE5NDA0MzctMzMwZi00ZDFjLWFhMTctNGJjMDgzYzAwN2ZkIn0.emP31LEdZ4dpdc7krFSADxmuKUTaCGAAjJZmEw78HFv1kR4l-3GcqT_C8EzHfIYycjhpIXi4Nop6kgedKw37DRcgutLdTYOd1WlG6rE90Xp0Y_rIdgmEPojHVlBj5Yh1JGOySDQWF2T83pV4kQGWK-EIbAcw4nJ0v9AmeYQGUJf9v0vRmXp_06uZK4K7saZWWNULldszk1RegJKs6CEOnsQ1E8W3xpILj7_K47WDkJLmRUyY4bqmfM5iNNNzIU3azCVvvgfA2kobyS4f_l1i9oP2K-mcDo049pix0JBepi9k38-ju_RgAC-iqhc6qhSSB7hfJD5NyIz8HFcPC1N8Wg&expires_in=3600&token_type=Bearer');

  // Go to https://www.dev.leasemgt.xl.co.id/
  await page.goto('https://www.dev.leasemgt.xl.co.id/');

  // Go to https://www.dev.leasemgt.xl.co.id/auth/login?id_token=eyJraWQiOiJaMkhPTTc2aTVZXC9RNEg4b0tLTGZFdWVKMDhWNVUrNVRWamZWU1wvNTBPUlU9IiwiYWxnIjoiUlMyNTYifQ.eyJjdXN0b206dGl0bGUiOiJMZWFkIEZpYmVyIiwiYXRfaGFzaCI6Im5uWDFQc0hMeVBEZFRQc2lLT0EwLVEiLCJjdXN0b206b3JnYW5pemF0aW9uIjoiWEwiLCJzdWIiOiJlMTk0MDQzNy0zMzBmLTRkMWMtYWExNy00YmMwODNjMDA3ZmQiLCJjdXN0b206Z3JvdXBpZCI6IkxFQURGSUJFUiIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3QtMS5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMV9rR2tDWmQ4TUwiLCJjb2duaXRvOnVzZXJuYW1lIjoiZTE5NDA0MzctMzMwZi00ZDFjLWFhMTctNGJjMDgzYzAwN2ZkIiwiYXVkIjoiZDRiMHA0dWl1MGQ1NjA1Z3Ezc29rdWxiOCIsImV2ZW50X2lkIjoiNDk0ODFiNjctODVlMi00YjljLTk5MTAtZmUwYjc0N2E0MzVlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MzI2NjEyMTEsIm5hbWUiOiJQVCBYTCBBeGlhdGEgVGJrIiwiZXhwIjoxNjMyNjY0ODExLCJpYXQiOjE2MzI2NjEyMTEsImVtYWlsIjoibG1zLmxlYWRmaWJlckB5YWhvby5jb20ifQ.FhkntoqO0DsHYqA-7C0lrYcvkz7mI8zrNpbVcrtSW2MgrJmajVmYd3jb-3laBoALz5fXQS2lh80VdZZwJHlWyFOSEwjXUv2anaRZoN95AK-sNDEYj9YYK9hDRaxWhgF4nv3Pvz7vOjKeeALW0VEmxQkTVR7VvfQP2tr6HoiN2FH_qRmFyhskxfjia4bJURZ3t2Ibn2YSmI66GHyF_1q5lMTusLO-sUMhZHShzCXwLcAG0LpbEhmgQQxuEgYZoYgNTJnQhRHUqOicnQP5V4n9_hLL0wfjV09z0jSiIOS5HW9tprxeLvFf0hvgD1vzd1a012FDWi8dJ146Kdqa5wrjgA
  await page.goto('https://www.dev.leasemgt.xl.co.id/auth/login?id_token=eyJraWQiOiJaMkhPTTc2aTVZXC9RNEg4b0tLTGZFdWVKMDhWNVUrNVRWamZWU1wvNTBPUlU9IiwiYWxnIjoiUlMyNTYifQ.eyJjdXN0b206dGl0bGUiOiJMZWFkIEZpYmVyIiwiYXRfaGFzaCI6Im5uWDFQc0hMeVBEZFRQc2lLT0EwLVEiLCJjdXN0b206b3JnYW5pemF0aW9uIjoiWEwiLCJzdWIiOiJlMTk0MDQzNy0zMzBmLTRkMWMtYWExNy00YmMwODNjMDA3ZmQiLCJjdXN0b206Z3JvdXBpZCI6IkxFQURGSUJFUiIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3QtMS5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMV9rR2tDWmQ4TUwiLCJjb2duaXRvOnVzZXJuYW1lIjoiZTE5NDA0MzctMzMwZi00ZDFjLWFhMTctNGJjMDgzYzAwN2ZkIiwiYXVkIjoiZDRiMHA0dWl1MGQ1NjA1Z3Ezc29rdWxiOCIsImV2ZW50X2lkIjoiNDk0ODFiNjctODVlMi00YjljLTk5MTAtZmUwYjc0N2E0MzVlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MzI2NjEyMTEsIm5hbWUiOiJQVCBYTCBBeGlhdGEgVGJrIiwiZXhwIjoxNjMyNjY0ODExLCJpYXQiOjE2MzI2NjEyMTEsImVtYWlsIjoibG1zLmxlYWRmaWJlckB5YWhvby5jb20ifQ.FhkntoqO0DsHYqA-7C0lrYcvkz7mI8zrNpbVcrtSW2MgrJmajVmYd3jb-3laBoALz5fXQS2lh80VdZZwJHlWyFOSEwjXUv2anaRZoN95AK-sNDEYj9YYK9hDRaxWhgF4nv3Pvz7vOjKeeALW0VEmxQkTVR7VvfQP2tr6HoiN2FH_qRmFyhskxfjia4bJURZ3t2Ibn2YSmI66GHyF_1q5lMTusLO-sUMhZHShzCXwLcAG0LpbEhmgQQxuEgYZoYgNTJnQhRHUqOicnQP5V4n9_hLL0wfjV09z0jSiIOS5HW9tprxeLvFf0hvgD1vzd1a012FDWi8dJ146Kdqa5wrjgA');

  // Go to https://www.dev.leasemgt.xl.co.id/dashboard
  await page.goto('https://www.dev.leasemgt.xl.co.id/dashboard');

  // Click text=Lease Capacity
  await page.click('text=Lease Capacity');

  // Click text=Upgrade & Downgrade
  await page.click('text=Upgrade & Downgrade');

  // Click :nth-match(:text("Review BAUT L1"), 2)
  await page.click(':nth-match(:text("Review BAUT L1"), 2)');

  // Go to https://www.dev.leasemgt.xl.co.id/lease-capacity/index-review-baut-l1-upgrade-downgrade
  await page.goto('https://www.dev.leasemgt.xl.co.id/lease-capacity/index-review-baut-l1-upgrade-downgrade');

  // Click text=202108076
  await page.click('text=202108076');
  // assert.equal(page.url(), 'https://www.dev.leasemgt.xl.co.id/lease-capacity/review-baut-l1-upgrade-downgrade/6088288c-a78a-46bf-8cf8-a5bc96f35fef');

  // ---------------------
  await context.close();
  await browser.close();
})();